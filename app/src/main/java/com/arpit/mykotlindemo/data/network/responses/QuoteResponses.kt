package com.arpit.mykotlindemo.data.network.responses

import com.arpit.mykotlindemo.data.db.entities.Quotes

data class QuoteResponses(
    val isSuccessful: Boolean,
    val quotes: List<Quotes>
)