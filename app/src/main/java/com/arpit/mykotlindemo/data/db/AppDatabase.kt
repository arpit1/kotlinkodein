package com.arpit.mykotlindemo.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.arpit.mykotlindemo.data.db.entities.Quotes
import com.arpit.mykotlindemo.data.db.entities.User

@Database(
    entities = [User::class, Quotes::class],
    version = 1
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun getUserDao(): UserDao
    abstract fun getQuoteDao(): QuoteDao

    companion object {
        //    @Volatile is used so that this variable is immediately visible to all other threads
        @Volatile
        private var instance: AppDatabase? = null
        //    it is use to check that two instances not get created for database
        private val LOCK = Any()

        operator fun invoke(context: Context) = instance ?: synchronized(LOCK) {
            instance ?: buildDatabase(context).also {
                instance = it
            }
        }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(
                context.applicationContext,
                AppDatabase::class.java,
                "Mydatabase.db"
            ).build()
    }
}