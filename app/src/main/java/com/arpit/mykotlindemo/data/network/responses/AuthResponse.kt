package com.arpit.mykotlindemo.data.network.responses

import com.arpit.mykotlindemo.data.db.entities.User

data class AuthResponse(
    val isSuccessful: Boolean?,
    val message: String?,
    val user: User?
)