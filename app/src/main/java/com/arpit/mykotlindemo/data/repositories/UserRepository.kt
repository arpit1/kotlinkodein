package com.arpit.mykotlindemo.data.repositories

import com.arpit.mykotlindemo.data.db.AppDatabase
import com.arpit.mykotlindemo.data.db.entities.User
import com.arpit.mykotlindemo.data.network.MyApi
import com.arpit.mykotlindemo.data.network.SafeApiRequest
import com.arpit.mykotlindemo.data.network.responses.AuthResponse

class UserRepository(
    private val api: MyApi,
    private val database: AppDatabase
) : SafeApiRequest() {

    suspend fun userLogin(email: String, password: String): AuthResponse {
        return apiRequest { api.userLogin(email, password) }
    }

    suspend fun userSignUp(name: String, email: String, password: String): AuthResponse {
        return apiRequest { api.userSignUp(name, email, password) }
    }


    suspend fun saveUser(user: User) = database.getUserDao().upsert(user)

    fun getUser() = database.getUserDao().getUser()
}