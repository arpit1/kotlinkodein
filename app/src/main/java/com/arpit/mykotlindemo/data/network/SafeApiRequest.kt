package com.arpit.mykotlindemo.data.network

import com.arpit.mykotlindemo.util.ApiException
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Response
import java.lang.StringBuilder

// This is a generic class for checking the response for the API's
abstract class SafeApiRequest {
    suspend fun <T : Any> apiRequest(call: suspend () -> Response<T>): T {
        val response = call.invoke()
        if(response.isSuccessful) {
            return response.body()!!
        } else {
            val error = response.errorBody()?.string()
            val message = StringBuilder()
//            it will get executed only if the error is not null
            error?.let {
                try {
//                Here we will be checking the error code if it is coming in the error response.
                    message.append(JSONObject(it).optString("message"))
                } catch (e : JSONException) {
                }
                message.append("\n")
            }
            message.append("Error Code: ${response.code()}")

            throw ApiException(message.toString())
        }
    }
}