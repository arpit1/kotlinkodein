package com.arpit.mykotlindemo.data.repositories

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.arpit.mykotlindemo.data.db.AppDatabase
import com.arpit.mykotlindemo.data.db.entities.Quotes
import com.arpit.mykotlindemo.data.network.MyApi
import com.arpit.mykotlindemo.data.network.SafeApiRequest
import com.arpit.mykotlindemo.data.preferences.PreferenceProvider
import com.arpit.mykotlindemo.util.Coroutines
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.time.LocalDateTime
import java.time.temporal.ChronoUnit

private const val MIN_INTERVAL = 6

class QuotesRepository(
    private val api: MyApi,
    private val db: AppDatabase,
    private val prefs: PreferenceProvider
) : SafeApiRequest() {

    private val quotes = MutableLiveData<List<Quotes>>()

    init {
        quotes.observeForever {
            saveQuotes(it)
        }
    }

    suspend fun getQuotes(): LiveData<List<Quotes>> {
        return withContext(Dispatchers.IO) {
            fetchQuotes()
            db.getQuoteDao().getQuotes()
        }
    }

    private suspend fun fetchQuotes() {
        val lastSavedAt = prefs.getLastSavedAt()
        if (lastSavedAt == null || isFetchNeeded(lastSavedAt)) {
            val response = apiRequest { api.getQuotes() }
            quotes.postValue(response.quotes)
        }
    }

    private fun isFetchNeeded(lastSavedAt: String): Boolean {
        return ChronoUnit.HOURS.between(LocalDateTime.parse(lastSavedAt), LocalDateTime.now()) > MIN_INTERVAL
    }

    private fun saveQuotes(quotes: List<Quotes>) {
        Coroutines.io {
            prefs.saveLastSavedAt(LocalDateTime.now().toString())
            db.getQuoteDao().saveAllQuotes(quotes)
        }
    }

}