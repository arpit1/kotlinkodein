package com.arpit.mykotlindemo.ui.home.quotes

import androidx.lifecycle.ViewModel
import com.arpit.mykotlindemo.data.repositories.QuotesRepository
import com.arpit.mykotlindemo.util.lazyDeferred

class QuotesViewModel(repository: QuotesRepository) : ViewModel() {

    val quotes by lazyDeferred {
        repository.getQuotes()
    }
}
