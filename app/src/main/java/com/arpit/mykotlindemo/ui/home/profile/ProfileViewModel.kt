package com.arpit.mykotlindemo.ui.home.profile

import androidx.lifecycle.ViewModel
import com.arpit.mykotlindemo.data.repositories.UserRepository

class ProfileViewModel(repository: UserRepository) : ViewModel() {

    val user = repository.getUser()
}
