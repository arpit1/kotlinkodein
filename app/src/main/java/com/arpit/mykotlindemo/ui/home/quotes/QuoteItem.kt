package com.arpit.mykotlindemo.ui.home.quotes

import com.arpit.mykotlindemo.R
import com.arpit.mykotlindemo.data.db.entities.Quotes
import com.arpit.mykotlindemo.databinding.ItemQuoteBinding
import com.xwray.groupie.databinding.BindableItem

class QuoteItem(
    private val quote: Quotes
) : BindableItem<ItemQuoteBinding>() {
    override fun getLayout() = R.layout.item_quote

    override fun bind(viewBinding: ItemQuoteBinding, position: Int) {
        viewBinding.setQuote(quote)
    }
}