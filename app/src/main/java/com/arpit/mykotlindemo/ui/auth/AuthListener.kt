package com.arpit.mykotlindemo.ui.auth

import com.arpit.mykotlindemo.data.db.entities.User

interface AuthListener {
    fun onStarted()
    fun onSuccess(user: User)
    fun onFailure(message : String)
}