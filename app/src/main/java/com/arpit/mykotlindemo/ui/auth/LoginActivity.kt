package com.arpit.mykotlindemo.ui.auth

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.arpit.mykotlindemo.R
import com.arpit.mykotlindemo.data.db.entities.User
import com.arpit.mykotlindemo.databinding.ActivityLoginBinding
import com.arpit.mykotlindemo.ui.home.HomeActivity
import com.arpit.mykotlindemo.util.hide
import com.arpit.mykotlindemo.util.show
import com.arpit.mykotlindemo.util.showToast
import com.arpit.mykotlindemo.util.snackbar
import kotlinx.android.synthetic.main.activity_login.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class LoginActivity : AppCompatActivity(), AuthListener, KodeinAware {

    override val kodein by kodein()

    private val factory: AuthViewModelFactory by instance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setBinding(savedInstanceState, factory)
    }

    private fun setBinding(savedInstanceState: Bundle?, factory: AuthViewModelFactory) {
        val binding: ActivityLoginBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_login)

        //        As we need to use the repository in the ViewModel so we have to pass the factory object
        val viewModel = ViewModelProviders.of(this, factory).get(AuthViewModel::class.java)
        binding.viewmodel = viewModel

        viewModel.authListener = this

        viewModel.getLoggedInUser().observe(this, Observer { user ->
            if (user != null) {
                Intent(this, HomeActivity::class.java).also {
                    startActivity(it)
                    finish()
                }

            }
        })

    }

    override fun onStarted() {
        progress_bar.show()
    }

    override fun onSuccess(user: User) {
        progress_bar.hide()
//        root_layout.snackbar("${user.name} is logged in")
        showToast("${user.name} is logged in")
    }

    override fun onFailure(message: String) {
        progress_bar.hide()
        root_layout.snackbar(message)
//        showToast(message)
    }
}
